# SYMFONY #
![img](foto.png)

# MANUAL #

[Referència al manual de DIGITAL OCEAN](https://www.digitalocean.com/community/tutorials/how-to-deploy-a-symfony-application-to-production-on-ubuntu-14-04)

# MODIFICACIONS FETES #

* En comptes de fer servir l'ordre
```
sudo apt-get install git php5-cli php5-curl acl
```
fem servir aquesta
```
sudo apt-get install git php-cli php-curl acl
```

* Despreés en comptes de afegir aquestes linies 
```
collation-server     = utf8mb4_general_ci # Replaces utf8_general_ci
character-set-server = utf8mb4            # Replaces utf8
```
a l'arxiu
```
/etc/mysql/my.cnf
```
hem de afegir-les en aquest a sota de BASIC SETTINGS
```
/etc/mysql/mysql.conf.d/mysqld.cnf
```
* També canviem la zona horaria a 
```
 etc/php5/fpm/php-ini
```
modifiquem 
```
date.timezone = Europe/Madrid
```
* Per últim a l'arxiu
```
 etc/apache2/sites-available/000-default.conf
```
modifiquem 
```
Order Allow,Deny
```
per 
```
Require all granted
```
i eliminem 
```
Allow from All
```
